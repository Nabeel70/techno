import { useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./vendor-profile.module.css";

const VendorProfile = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (
    <>
      <main className={styles.vendorProfileMain} id="main">
        <img
          className={styles.rectangleIcon}
          alt=""
          src="../rectangle-161@2x.png"
        />
              <button className={styles.iconButton}>
      <div className={styles.arrowForwardIosIcon}>{'<'}</div>
    </button>
    <button className={styles.iconButton1}>
    <div className={styles.arrowForwardIosIcon1}>{'<'}</div>
    </button>
        <h2 className={styles.alphaCostumesVendor}>
          <b>
            <span>Alpha Costumes </span>
            <span className={styles.span}>{`• `}</span>
          </b>
          <span className={styles.span}>
            <i className={styles.vendorI}>Vendor</i>
          </span>
        </h2>
        <h6
          className={styles.alphaCostumesMakesPremiumC}
        >{`Alpha Costumes makes premium cosplay for clients across the world; we’ve been featured in numerous magazines such as Apollo and Sci-Fi. `}</h6>
        <h6 className={styles.alphacosplaycarrdcoH6}>alphacosplay.carrd.co</h6>
        
        <img className={styles.vectorIcon} alt="" src="../vector1.svg" />
        <img className={styles.vectorIcon1} alt="" src="../vector2.svg" />
        <img className={styles.vectorIcon2} alt="" src="../vector3.svg" />

      
        <p className={styles.loremIpsumDolorSitAmetCo}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris adipiscing
          elit, sed do eiusmod tempor incididunt ut labore et dolore magna
          aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
          laboris.
        </p>
        <img className={styles.ellipseIcon} alt="" src="../ellipse-1@2x.png" />
        <h6 className={styles.clothingCraftsArtist}>
          #Clothing #Crafts #Artist
        </h6>
        <div className={styles.lineDiv} />
        <h6 className={styles.seeUpcomingCollaborations}>
          See Upcoming Collaborations
        </h6>
        <p className={styles.clickOnTheCollaborationTo}>
          Click on the collaboration to see the map location.
        </p>
        <div className={styles.frameDiv}>
          <img
            className={styles.rectangleIcon1}
            alt=""
            src="../rectangle-15@2x.png"
          />
          <h3 className={styles.jamesCafeH3}>James’ Cafe</h3>
          <h6 className={styles.shelfSpaceH6}>Shelf Space</h6>
          <h6 className={styles.h6}>08/04/2022 - 08/05/2022</h6>
        </div>
        <div className={styles.frameDiv1}>
          <img
            className={styles.rectangleIcon1}
            alt=""
            src="../rectangle-152@2x.png"
          />
          <h3 className={styles.jamesCafeH3}>Welcome Bicycles</h3>
          <h6 className={styles.counterSpace2}>counter space 2</h6>
          <h6 className={styles.h61}>08/04/2022 - 08/05/2022</h6>
        </div>
        <div className={styles.footerDiv}>
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv1} />
        </div>
        <div className={styles.frameDiv2}>
          <Link href="/">
            <a className={styles.shareAshelf} onClick={onShareAshelfLinkClick}>
              <span className={styles.shareAshelfTxtSpan}>
                <span className={styles.shareSpan}>share</span>
                <span className={styles.aSpan}>A</span>
                <span className={styles.shelfSpan}>shelf</span>
              </span>
            </a>
          </Link>
          <button className={styles.groupButton} onClick={openMenuDrawer}>
            <button className={styles.groupButton1}>
              <div className={styles.inputChipDiv}>
                <img
                  className={styles.userImagesUserImages}
                  alt=""
                  src="../user-imagesuser-images.svg"
                />
                <div className={styles.labelTextDiv}>Enabled</div>
              </div>
              <img className={styles.menuIcon} alt="" />
              <img className={styles.menuIcon1} alt="" src="../menu.svg" />
            </button>
          </button>
          <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
          <Link href="/signup">
            <a className={styles.signUpA} onClick={onSignUpLinkClick}>
              Sign Up:
            </a>
          </Link>
        </div>
      </main>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default VendorProfile;
