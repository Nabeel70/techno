import { useState, useCallback } from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./hosts-profile.module.css";

const HostsProfile = () => {
  const router = useRouter();
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onShareAshelfLinkClick = useCallback(() => {
    router.push("/");
  }, [router]);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  const onSignUpLinkClick = useCallback(() => {
    router.push("/signup");
  }, [router]);

  return (
    <>
      <main className={styles.hostsProfileMain} id="main">
        <img
          className={styles.rectangleIcon}
          alt=""
          src="../rectangle-16@2x.png"
        />
         <button className={styles.iconButton}>
      <div className={styles.arrowForwardIosIcon}>{'<'}</div>
    </button>
    <button className={styles.iconButton1}>
    <div className={styles.arrowForwardIosIcon1}>{'<'}</div>
    </button>
        <h2 className={styles.checkmateHost}>
          <b>
            <span>Checkmate </span>
            <span className={styles.span}>{`• `}</span>
          </b>
          <span className={styles.span}>
            <i className={styles.hostI}>Host</i>
          </span>
        </h2>
        <h6 className={styles.checkmateIsASmallCafeLoca}>
          Checkmate is a small cafe located in the heart of Edinburgh’s Historic
          New Town. We specialise in teas and boardgames from around the world
          and complement these with an ever changing selection of cakes.
        </h6>
        <h6 className={styles.samscafeedinburghcomH6}>samscafeedinburgh.com</h6>
        <img className={styles.vectorIcon} alt="" src="../vector.svg" />
        <p className={styles.loremIpsumDolorSitAmetCo}>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
          eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad
          minim veniam, quis nostrud exercitation ullamco laboris adipiscing
          elit, sed do eiusmod tempor incididunt ut labore et dolore magna
          aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco
          laboris.
        </p>
        <img className={styles.ellipseIcon} alt="" src="../ellipse-1@2x.png" />
        <div className={styles.lineDiv} />
        <h6 className={styles.seeAvailableSpaces}>See Available Spaces</h6>
        <p className={styles.clickOnTheSpaceToSeeMore}>
          Click on the space to see more details and submit a booking request.
        </p>
        <div className={styles.frameDiv}>
          <img
            className={styles.rectangleIcon1}
            alt=""
            src="../rectangle-15@2x.png"
          />
          <h6 className={styles.shelfSpaceH6}>Shelf Space</h6>
          <p className={styles.jamesCafeP}>James’ Cafe</p>
        </div>
        <h6 className={styles.cafeH6}>#Cafe</h6>
        <div className={styles.lineDiv1} />
        <h1 className={styles.seeUpcomingCollaborations}>
          See Upcoming Collaborations
        </h1>
        <h2 className={styles.clickOnTheCollaborationTo}>
          Click on the collaboration to see the map location.
        </h2>
        <div className={styles.frameDiv1}>
          <img
            className={styles.rectangleIcon1}
            alt=""
            src="../rectangle-15@2x.png"
          />
          <h2 className={styles.shelfSpaceH6}>Alpha Costumes</h2>
          <h2 className={styles.shelfSpaceH2}>shelf space</h2>
          <p className={styles.p}>08/04/2022 - 08/05/2022</p>
        </div>
        <div className={styles.frameDiv2}>
          <img
            className={styles.rectangleIcon1}
            alt=""
            src="../rectangle-152@2x.png"
          />
          <h2 className={styles.shelfSpaceH6}>Register Counter</h2>
          <h2 className={styles.shelfSpaceH2}>welcome bicycles</h2>
          <p className={styles.p1}>08/04/2022 - 08/05/2022</p>
        </div>
        <div className={styles.footerDiv}>
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv2} />
        </div>
        <div className={styles.frameDiv3}>
          <Link href="/">
            <a className={styles.shareAshelf} onClick={onShareAshelfLinkClick}>
              <span className={styles.shareAshelfTxtSpan}>
                <span className={styles.shareSpan}>share</span>
                <span className={styles.aSpan}>A</span>
                <span className={styles.shelfSpan}>shelf</span>
              </span>
            </a>
          </Link>
          <button className={styles.groupButton} onClick={openMenuDrawer}>
            <div className={styles.groupDiv}>
              <div className={styles.inputChipDiv}>
                <img
                  className={styles.userImagesUserImages}
                  alt=""
                  src="../user-imagesuser-images.svg"
                />
                <div className={styles.labelTextDiv}>Enabled</div>
              </div>
              <img className={styles.menuIcon} alt="" />
              <img className={styles.menuIcon1} alt="" src="../menu.svg" />
            </div>
          </button>
          <h6 className={styles.whatIsShareAshelf}>what is shareAshelf?</h6>
          <Link href="/signup">
            <a className={styles.signUpA} onClick={onSignUpLinkClick}>
              Sign Up:
            </a>
          </Link>
        </div>
      </main>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default HostsProfile;
