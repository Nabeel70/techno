import { useState, useCallback } from "react";
import Link from "next/link";
import MenuDrawer from "../components/menu-drawer";
import PortalDrawer from "../components/portal-drawer";
import styles from "./faqs.module.css";

const Faqs = () => {
  const [isMenuDrawerOpen, setMenuDrawerOpen] = useState(false);

  const onIconButtonClick = useCallback(() => {
    const anchor = document.querySelector(
      "[data-scroll-to='rectangleContainer']"
    );
    if (anchor) {
      anchor.scrollIntoView({ block: "start", behavior: "smooth" });
    }
  }, []);

  const onIconButton1Click = useCallback(() => {
    const anchor = document.querySelector(
      "[data-scroll-to='rectangleContainer1']"
    );
    if (anchor) {
      anchor.scrollIntoView({ block: "start", behavior: "smooth" });
    }
  }, []);

  const onIconButton2Click = useCallback(() => {
    const anchor = document.querySelector(
      "[data-scroll-to='rectangleContainer2']"
    );
    if (anchor) {
      anchor.scrollIntoView({ block: "start", behavior: "smooth" });
    }
  }, []);

  const onIconButton3Click = useCallback(() => {
    const anchor = document.querySelector(
      "[data-scroll-to='rectangleContainer3']"
    );
    if (anchor) {
      anchor.scrollIntoView({ block: "start", behavior: "smooth" });
    }
  }, []);

  const openMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(true);
  }, []);

  const closeMenuDrawer = useCallback(() => {
    setMenuDrawerOpen(false);
  }, []);

  return (
    <>
      <main className={styles.faqsMain} id="main">
        <div className={styles.rectangleDiv} />
        <div className={styles.rectangleDiv1} />
        <div className={styles.rectangleDiv2} data-scroll-to='rectangleContainer2' />
        <div className={styles.rectangleDiv3} data-scroll-to="rectangleContainer1" />
        <div
          className={styles.rectangleDiv4}
          data-scroll-to="rectangleContainer"
        />
        <div className={styles.whatCanShareAshelfDoForYo}>
          <span>
            <b>{`what can `}</b>
            <span className={styles.shareSpan}>share</span>
            <span className={styles.aSpan}>A</span>
            <span className={styles.shareSpan}>shelf</span>
          </span>
          <b>
            <span className={styles.span}>{` `}</span>
            <span>do for you?</span>
          </b>
        </div>
        <div className={styles.howDoesRentmyshelfWork} data-scroll-to='rectangleContainer3'>
          <span>{`how does `}</span>
          <span className={styles.span}>{`rentmyshelf `}</span>
          <span>work?</span>
        </div>
        <p className={styles.shareAshelfHelpsHobbyistsR}>
          shareAshelf helps hobbyists, retail entrepeneurs, and makers get their
          products into physical locations by connecting them with hosts that
          have spare space. This can be as simple as displaying their work on a
          shelf or wall in a cafe, or as involved as setting up a stall at an
          event.
        </p>
        <div className={styles.ifYouLikeToShopLocalUma}>
          <p className={styles.ifYouLikeToShopLocal}>
            <span>if you like to shop local:</span>
          </p>
          <p className={styles.umarketEnablesYouToDiscove}>
            <span>umarket enables you to discover new stores</span>
          </p>
        </div>
        <div className={styles.ifYouHaveSpaceThatPeople}>
          <p className={styles.ifYouLikeToShopLocal}>
            <span>if you have space that people pass through:</span>
          </p>
          <p className={styles.umarketEnablesYouToDiscove}>
            <span>
              umarket connects you with amazing vendors to collaborate with
            </span>
          </p>
        </div>
        <div className={styles.ifYouHaveSomethingToSell}>
          <p className={styles.ifYouLikeToShopLocal}>
            <span>if you have something to sell:</span>
          </p>
          <p className={styles.umarketEnablesYouToDiscove}>
            <span>
              rentmyshelf connects you with unique places to sell your products
            </span>
          </p>
        </div>

        <div className={styles.maybeYouSellHandmadeCosmet}>
          <p className={styles.ifYouLikeToShopLocal}>
            Maybe you sell handmade cosmetics, or you source rare sneakers, or
            perhaps you are a local artist looking to get your work shown in
            more places? umarket lets you:
          </p>
          <p className={styles.ifYouLikeToShopLocal}>&nbsp;</p>
          <p className={styles.ifYouLikeToShopLocal}>search available hosts</p>
          <p
            className={styles.ifYouLikeToShopLocal}
          >{`book space with hosts `}</p>
          <p className={styles.dealsWithPayment}>
            deals with payment so you don’t have to
          </p>
        </div>
 
        
        <div className={styles.largeFABDiv}>
          <div className={styles.containerDiv}>
            <h2 className={styles.iHaveSomethingToSell}>
              I have something to sell.
            </h2>
          </div>
        </div>
        <div className={styles.containerDiv1}>
          <h2 className={styles.iHaveSomethingToSell0}>I like to shop.</h2>
        </div>
        <div className={styles.largeFABDiv1}>
          <div className={styles.containerDiv2}>
            <h2 className={styles.iHaveSomethingToSell}>
              I have a space that people visit or walk through.
            </h2>
          </div>
        </div>
        <div className={styles.youMayBeAHairSalonOrYo}>
          <p className={styles.ifYouLikeToShopLocal}>
            You may be a hair salon, or you may be a cafe, or perhaps you are a
            community centre? Maybe you are museum, or maybe you rent out a part
            of your house to guests?
          </p>
          <p className={styles.ifYouLikeToShopLocal}>umarket...</p>
          <p className={styles.ifYouLikeToShopLocal}>
            utilise your unused space to earn money
          </p>
          <p className={styles.ifYouLikeToShopLocal}>
            inivigorate your space by collaborating with interesting vendors
          </p>
          <p className={styles.dealsWithPayment}>
            handles all payment so you don’t have to
          </p>
        </div>
        <div className={styles.perhapsYouWantToShopMore}>
          <p
            className={styles.ifYouLikeToShopLocal}
          >{`Perhaps you want to shop more locally, but find it hard to know what is around you? Perhaps you enjoy supporting small businesses. Perhaps you love discovering new surprises while you shop. `}</p>
          <p className={styles.ifYouLikeToShopLocal}>umarket</p>
          <p className={styles.ifYouLikeToShopLocal}>
            Provides you with a constantly changing set of collaborations for
            you to discover
          </p>
          <p className={styles.ifYouLikeToShopLocal}>
            Makes you aware of all the non-physical retail vendors around you.
          </p>
          <p
            className={styles.dealsWithPayment}
          >{`Allows you to connect with your favourite online entrepeneurs and makers. `}</p>
        </div>
        <div className={styles.lineDiv} />
        <div className={styles.lineDiv1} />
        <div className={styles.lineDiv2} />
        <div className={styles.whatIsShareAshelf}>
          <b>{`what is `}</b>
          <span className={styles.shareSpan1}>share</span>
          <span className={styles.aSpan1}>A</span>
          <span className={styles.shelfSpan1}>
            <span>shelf</span>
            <span className={styles.span1}>{` `}</span>
          </span>
          <b>?</b>
        </div>
        <button className={styles.iconButton} onClick={onIconButtonClick}>
          <img
            className={styles.arrowForwardIosIcon}
            alt=""
            src="../arrow-forward-ios3.svg"
          />
        </button>
        <h6 className={styles.letsGetYouConnected}>let’s get you connected</h6>
        <button className={styles.iconButton1} onClick={onIconButton1Click}>
          <img
            className={styles.arrowForwardIosIcon}
            alt=""
            src="../arrow-forward-ios4.svg"
          />
        </button>
        <p className={styles.howDoesItWork}>how does it work?</p>
        <button className={styles.iconButton2} onClick={onIconButton2Click}>
          <img
            className={styles.arrowForwardIosIcon}
            alt=""
            src="../arrow-forward-ios5.svg"
          />
        </button>
        <p className={styles.howDoesItWork1}>how does it work?</p>
        <button className={styles.iconButton3} onClick={onIconButton3Click}>
          <img
            className={styles.arrowForwardIosIcon}
            alt=""
            src="../arrow-forward-ios6.svg"
          />
        </button>
        <p className={styles.howDoesItWork2}>how does it work?</p>
        <img className={styles.paintingIcon} alt="" src="../painting.svg" />
        <img
          className={styles.salesPersonIcon}
          alt=""
          src="../sales-person.svg"
        />
        <img
          className={styles.shareLocationIcon}
          alt=""
          src="../share-location.svg"
        />
        <img
          className={styles.buildACommunity}
          alt=""
          src="../build-a-community.svg"
        />
        <img
          className={styles.bookAppointmentIcon}
          alt=""
          src="../book-appointment.svg"
        />
        <div className={styles.noUpfrontFeesOrRentAComi}>
          <p className={styles.ifYouLikeToShopLocal}>no upfront fees or rent</p>
          <p className={styles.ifYouLikeToShopLocal}>
            a comission on sales is split between umarket and the host; your
            success is our success
          </p>
          <p className={styles.ifYouLikeToShopLocal}>&nbsp;</p>
          <p className={styles.ifYouLikeToShopLocal}>
            simple phone-based payment system
          </p>
          <p className={styles.dealsWithPayment}>
            no worrying about integrating products into existing host’s payment
            systems
          </p>
        </div>
        <div className={styles.headerMainsiteDiv}>
          <Link href="/">
            <a className={styles.shareAshelf}>
              <span className={styles.shareAshelfTxtSpan}>
                <span className={styles.shareSpan2}>share</span>
                <span className={styles.aSpan2}>A</span>
                <span className={styles.shelfSpan1}>shelf</span>
              </span>
            </a>
          </Link>
          <button className={styles.groupButton} onClick={openMenuDrawer}>
            <div className={styles.groupDiv}>
              <div className={styles.inputChipDiv}>
                <img
                  className={styles.userImagesUserImages}
                  alt=""
                  src="../user-imagesuser-images.svg"
                />
                <div className={styles.labelTextDiv}>Enabled</div>
              </div>
              <img className={styles.menuIcon} alt="" />
              <img className={styles.menuIcon1} alt="" src="../menu.svg" />
            </div>
          </button>
          <h6 className={styles.whatIsShareAshelf1}>what is shareAshelf?</h6>
          <Link href="/signup">
            <a className={styles.signUpA}>Sign Up:</a>
          </Link>
        </div>
        <div className={styles.formFooterDiv}>
          <div className={styles.rectangleDiv5} />
          <div className={styles.rentmyshelfIncDiv}>
            © 2022 rentmyshelf, Inc.
          </div>
          <div className={styles.supportDiv}>Support</div>
          <div className={styles.contactDiv}>Contact</div>
          <div className={styles.termsOfService}>Terms of Service</div>
          <div className={styles.lineDiv3} />
        </div>
      </main>
      {isMenuDrawerOpen && (
        <PortalDrawer
          overlayColor="rgba(113, 113, 113, 0.3)"
          placement="Right"
          onOutsideClick={closeMenuDrawer}
        >
          <MenuDrawer onClose={closeMenuDrawer} />
        </PortalDrawer>
      )}
    </>
  );
};

export default Faqs;
